package com.emirilda.spigotmc.emirilda.commands;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.EnumSet;

public enum AllowedCommandSender {
    CONSOLE(ConsoleCommandSender.class),
    PLAYER(Player.class),
    BLOCK(BlockCommandSender.class);

    public static final EnumSet<AllowedCommandSender> ALL = EnumSet.allOf(AllowedCommandSender.class);
    public static final EnumSet<AllowedCommandSender> PHYSICAL = EnumSet.of(AllowedCommandSender.PLAYER, AllowedCommandSender.BLOCK);
    public static final EnumSet<AllowedCommandSender> MANUAL = EnumSet.of(AllowedCommandSender.PLAYER, AllowedCommandSender.CONSOLE);

    public Class<? extends CommandSender> getSenderClass() {
        return senderClass;
    }

    protected Class<? extends CommandSender> senderClass;

    public boolean isSenderAllowed(Class<? extends CommandSender> senderClass) {
        return this.senderClass.isAssignableFrom(senderClass);
    }

    AllowedCommandSender(Class<? extends CommandSender> senderClass) {
        this.senderClass = senderClass;
    }
}
