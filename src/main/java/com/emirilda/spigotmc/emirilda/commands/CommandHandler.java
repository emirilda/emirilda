package com.emirilda.spigotmc.emirilda.commands;

import com.google.common.base.Joiner;
import com.emirilda.spigotmc.emirilda.commands.arguments.LiteralCommandArgument;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.stream.Collectors;

import static net.md_5.bungee.api.ChatColor.*;

public class CommandHandler  implements CommandExecutor, TabCompleter {

    private JavaPlugin plugin;

    private List<LiteralCommandArgument> commands = new ArrayList<>();

    public CommandHandler(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    public LiteralCommandArgument addCommand(String label) {
        PluginCommand cmd = plugin.getCommand(label);
        assert cmd != null;
        cmd.setExecutor(this);
        cmd.setTabCompleter(this);

        LiteralCommandArgument command = (LiteralCommandArgument) new LiteralCommandArgument(label)
                .withMeta(
                        CommandArgumentMeta
                                .builder()
                                .description(cmd.getDescription())
                                .permissions(CommandArgumentMeta.decodePermissionString(cmd.getPermission()))
                                .build()
                );
        commands.add(command);
        return command;

    }

    public LiteralCommandArgument addCommand(LiteralCommandArgument command, String label){
        PluginCommand cmd = plugin.getCommand(label);
        assert cmd != null;
        cmd.setExecutor(this);
        cmd.setTabCompleter(this);

        commands.add(command);
        return command;
    }

    public LiteralCommandArgument getCommand(String label) {
        for (LiteralCommandArgument command : commands) {
            if (command.getArgumentName().equalsIgnoreCase(label))
                return command;
        }
        return null;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        List<String> argsList = new ArrayList<>();
        argsList.add(command.getLabel());
        Collections.addAll(argsList, args);

        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("auto:label", label);
        namedParameters.put("auto:path", argsList);

        for (LiteralCommandArgument cmd : commands) {
            if (cmd.run(sender, command, argsList, namedParameters))
                return true;
        }

        sender.sendMessage(ChatColor.RED + "Couldn't find the command you're looking for. :(");

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        List<String> argsList = new ArrayList<>();
        argsList.add(command.getLabel());
        Collections.addAll(argsList, args);

        for (LiteralCommandArgument cmd : commands) {
            if (cmd.getArgumentName().equalsIgnoreCase(command.getLabel())) {
                List<String> completions = cmd.tabComplete(sender, argsList);
                return completions.stream().filter(s -> {
                    // slightly cleaner way of doing String.startsWith()
                    String lastArg = args[args.length - 1];
                    return s.regionMatches(true, 0, lastArg, 0, lastArg.length());
                }).sorted().collect(Collectors.toCollection(ArrayList::new));
            }
        }
        return null;
    }

    public Map<String, CommandArgumentMeta> getPossiblePaths(CommandSender sender, List<String> args) {
        Map<String, CommandArgumentMeta> helpStrings = new LinkedHashMap<>();
        for (LiteralCommandArgument cmd : commands) {
            helpStrings.putAll(cmd.getPossiblePaths(sender, args));
        }
        return helpStrings;
    }

    public boolean sendHelp(CommandSender sender, Command command, Map<String, Object> namedParameters) {
        List<String> helpPath = new ArrayList<>();
        helpPath.add(command.getLabel());
        //noinspection unchecked
        helpPath.addAll((List<String>) namedParameters.get("auto:overflow"));
        Map<String, CommandArgumentMeta> paths = getPossiblePaths(sender, helpPath);

        ComponentBuilder builder = new ComponentBuilder("-- Help for \"" + Joiner.on(' ').join(helpPath) + "\" --")
                .color(DARK_AQUA)
                .bold(true);

        for (Map.Entry<String, CommandArgumentMeta> path : paths.entrySet()) {
            ComponentBuilder hoverBuilder = new ComponentBuilder(path.getKey())
                    .color(DARK_PURPLE)
                    .bold(true)
                    .append("\nDescription: ")
                    .append(path.getValue().getDescription())
                    .color(LIGHT_PURPLE)
                    .bold(false)
                    .append("\n\nRequires permission: ")
                    .append(Joiner.on(", ").join(path.getValue().getPermissions()))
                    .color(GRAY)
                    .bold(false);
            builder
                    .append("\n/" + path.getKey())
                    .color(AQUA)
                    .bold(false)
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverBuilder.create()))
                    .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/"+path.getKey()));
        }
        sender.spigot().sendMessage(builder.create());
        return true;
    }

}
