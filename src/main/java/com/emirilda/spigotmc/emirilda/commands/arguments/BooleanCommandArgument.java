package com.emirilda.spigotmc.emirilda.commands.arguments;

import com.emirilda.spigotmc.emirilda.commands.CommandArgument;

import java.util.Arrays;
import java.util.List;

public class BooleanCommandArgument extends CommandArgument<Boolean> {

    public BooleanCommandArgument(String argumentName) {
        super(argumentName, "boolean");
    }

    @Override
    public List<String> getTabCompletions() {
        return Arrays.asList("true", "false");
    }

    @Override
    public boolean validate(String argument) {
        return argument.equalsIgnoreCase("true") || argument.equalsIgnoreCase("false");
    }

    @Override
    public Boolean convertToType(String argument) {
        return argument.equalsIgnoreCase("true");
    }

}
