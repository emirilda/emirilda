package com.emirilda.spigotmc.emirilda.commands.builder;

import com.emirilda.spigotmc.emirilda.commands.CommandHandler;
import lombok.Getter;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Set;

public class CommandBuilder {

    private JavaPlugin plugin;
    @Getter
    private HashMap<String, Method> commandHooks;
    @Getter
    private HashMap<String, Object> commandHookObjects;
    @Getter
    private HashMap<String, YamlCommand> commands;
    @Getter
    private CommandHandler handler;

    public CommandBuilder(JavaPlugin plugin){
        this.plugin = plugin;
        commandHooks = new HashMap<>();
        commands = new HashMap<>();
        commandHookObjects = new HashMap<>();
        handler = new CommandHandler(plugin);
    }

    public void load(String resourceName){

        InputStream inputStream = plugin.getResource(resourceName);
        Reader reader = new InputStreamReader(inputStream);
        YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(reader);

        MemorySection section = (MemorySection) yamlConfiguration.get("");
        Set<String> paths = section.getKeys(false);

        paths.forEach(path -> {
            commands.put(path, YamlCommand.loadFromYaml(yamlConfiguration, path));
        });

    }

    public void build(){
        commands.forEach((string, yamlCommand) -> {
            yamlCommand.addToHandler(handler, this);
            yamlCommand.getArgs().forEach((s, yamlArgument) -> {
                yamlArgument.addToHandler(handler.getCommand(string), this);
                yamlArgument.addChildrenToHandler(this);
            });
        });
    }

    public void addCommandHook(Object commandHook){
        Class commandHookClass = commandHook.getClass();
            for (Method declaredMethod : commandHookClass.getDeclaredMethods()) {
                if(declaredMethod.isAnnotationPresent(CommandHookExecutor.class)){
                    declaredMethod.setAccessible(true);
                    commandHooks.put(declaredMethod.getAnnotation(CommandHookExecutor.class).value(), declaredMethod);
                    commandHookObjects.put(declaredMethod.getAnnotation(CommandHookExecutor.class).value(), commandHook);
                }
            }
    }


}
