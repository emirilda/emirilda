package com.emirilda.spigotmc.emirilda.commands.arguments;

import com.emirilda.spigotmc.emirilda.commands.CommandArgument;

import java.util.ArrayList;
import java.util.List;

public class DoubleCommandArgument extends CommandArgument<Double> {

    public DoubleCommandArgument(String argumentName) {
        super(argumentName, "double");
    }

    @Override
    public List<String> getTabCompletions() {
        return new ArrayList<>();
    }

    @Override
    public boolean validate(String argument) {
        if (argument.startsWith("-")) {
            argument = argument.substring(1);
        }
        boolean containsDecimal = false;
        for (char c : argument.toCharArray()) {
            if (c == '.') {
                if (containsDecimal)
                    return false;
                containsDecimal = true;
            } else if (!Character.isDigit(c))
                return false;
        }
        return true;
    }

    @Override
    public Double convertToType(String argument) {
        return Double.valueOf(argument);
    }

}
