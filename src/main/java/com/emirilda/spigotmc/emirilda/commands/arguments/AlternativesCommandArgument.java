package com.emirilda.spigotmc.emirilda.commands.arguments;

import com.emirilda.spigotmc.emirilda.commands.CommandArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AlternativesCommandArgument extends CommandArgument<String> {
    private final Set<String> alternatives;
    private final String argumentHelp;

    public AlternativesCommandArgument(String argumentName, String argumentDisplayType, Set<String> alternatives) {
        super(argumentName);
        this.alternatives = alternatives;
        if (argumentDisplayType != null) {
            this.argumentDisplayType = argumentDisplayType;
            this.argumentHelp = null;
        } else {
            this.argumentHelp = alternatives.stream().sorted().collect(Collectors.joining(" | "));
        }
    }

    public AlternativesCommandArgument(String argumentName, Set<String> alternatives) {
        this(argumentName, null, alternatives);
    }

    @Override
    public List<String> getTabCompletions() {
        return new ArrayList<>(alternatives);
    }

    @Override
    public boolean validate(String argument) {
        return alternatives.stream().anyMatch(argument::equalsIgnoreCase);
    }

    @Override
    public String convertToType(String argument) {
        return alternatives.stream().filter(argument::equalsIgnoreCase).findFirst().orElse("");
    }

    @Override
    public String getArgumentHelp() {
        if (argumentHelp != null)
            return argumentHelp;
        return super.getArgumentHelp();
    }
}
