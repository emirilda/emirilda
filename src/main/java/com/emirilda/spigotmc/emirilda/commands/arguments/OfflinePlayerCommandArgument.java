package com.emirilda.spigotmc.emirilda.commands.arguments;

import com.emirilda.spigotmc.emirilda.Emirilda;
import com.emirilda.spigotmc.emirilda.commands.CommandArgument;
import org.bukkit.OfflinePlayer;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OfflinePlayerCommandArgument extends CommandArgument<String> {
    public OfflinePlayerCommandArgument(String argumentName) {
        super(argumentName, "player");
    }

    @Override
    public List<String> getTabCompletions() {
        return Arrays.stream(Emirilda.getInstance().getServer().getOfflinePlayers()).map(OfflinePlayer::getName).collect(Collectors.toList());
    }

    @Override
    public boolean validate(String argument) {
        return true;
    }

    @Override
    public String convertToType(String argument) {
        return argument;
    }
}
