package com.emirilda.spigotmc.emirilda.commands;

import com.emirilda.spigotmc.emirilda.utility.TextUtility;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class CommandArgument<TClass> {

    public CommandArgument(String argumentName, @Nullable String argumentDisplayType) {
        this.argumentName = argumentName;
        this.argumentDisplayType = argumentDisplayType;
    }

    public CommandArgument(String argumentName) {
        this(argumentName, null);
    }

    protected CommandArgument<?> parent = null;

    private CommandArgument<TClass> withParent(CommandArgument<?> parent) {
        this.parent = parent;
        return this;
    }

    public final CommandArgument<?> getParent() {
        return parent;
    }

    protected List<CommandArgument<?>> children = new ArrayList<>();

    public final CommandArgument<TClass> then(CommandArgument<?> argument) {
        children.add(argument.withParent(this));
        return this;
    }

    protected CommandArgumentMeta meta = new CommandArgumentMeta();

    public final CommandArgument<TClass> withMeta(CommandArgumentMeta meta) {
        if (meta != null)
            this.meta = meta;
        return this;
    }

    protected String argumentName;
    protected String argumentDisplayType;

    public final CommandArgumentMeta getMeta() {
        return meta;
    }

    public final String getArgumentName() {
        return argumentName;
    }

    public final String getArgumentDisplayType() {
        return argumentDisplayType;
    }

    public abstract List<String> getTabCompletions();

    public abstract boolean validate(String argument);

    public abstract TClass convertToType(String argument);

    public String getArgumentHelp() {
        return String.format(argumentDisplayType == null ? "<%s>" : "<%s: %s>", argumentName, argumentDisplayType);
    }

    protected CommandExecution execution;

    public final CommandArgument<TClass> executes(CommandExecution execution) {
        this.execution = execution;
        return this;
    }

    public final CommandExecution getExecution() {
        return execution;
    }

    public final boolean run(CommandSender sender, Command command, List<String> arguments, Map<String, Object> namedParameters) {
        String current = arguments.get(0);
        if (validate(current)) {
            List<String> rest =
                    arguments.size() > 1
                            ? arguments.subList(1, arguments.size())
                            : null;

            if (meta.isSenderPermitted(sender)) {
                namedParameters.put(argumentDisplayType, convertToType(current));

                if (rest == null || children.isEmpty()) {
                    if (execution != null) {
                        if (!meta.isSenderAllowed(sender.getClass())) {
                            List<String> senders = new ArrayList<>();
                            for (AllowedCommandSender s : meta.getAllowedSenders()) {
                                senders.add(TextUtility.pluralize(s.toString().toLowerCase()));
                            }
                            sender.sendMessage(ChatColor.RED + "Only " + TextUtility.readableJoin(senders.toArray(new String[0])) + " are allowed to run this command!");

                            return true;
                        }
                        namedParameters.put("auto:overflow",
                                rest != null
                                        ? Collections.unmodifiableList(rest)
                                        : Collections.emptyList()
                        );
                        execution.execute(sender, command, namedParameters);
                        return true;
                    }
                    return false;
                }
                for (CommandArgument<?> child : children) {
                    if (child.run(sender, command, rest, namedParameters)) {
                        return true;
                    }
                }
            }else {
                sender.sendMessage(ChatColor.RED + "You don't have the required permission for this command!");
                return true;
            }
        }
        return false;
    }

    public final List<String> tabComplete(CommandSender sender, List<String> arguments) {
        if (meta.isSenderPermitted(sender)) {
            String current = arguments.size() > 0 ? arguments.get(0) : null;
            List<String> rest = arguments.size() > 1 ? arguments.subList(1, arguments.size()) : null;

            if ((current == null || rest == null) && meta.isSenderAllowed(sender.getClass())) {
                return getTabCompletions();
            }

            if (current != null && validate(current)) {
                List<String> completions = new ArrayList<>();
                for (CommandArgument<?> child : children) {
                    completions.addAll(child.tabComplete(sender, rest));
                }
                return completions;
            }
        }

        return Collections.emptyList();
    }

    private Map<String, CommandArgumentMeta> getPossiblePathsInternal(CommandSender sender, List<String> arguments, Set<String> permissionBubbling) {
        if (meta.isSenderPermitted(sender)) {
            String current = arguments.size() > 0 ? arguments.get(0) : null;
            List<String> rest = arguments.size() > 1 ? arguments.subList(1, arguments.size()) : Collections.emptyList();

            boolean isValid = current != null && validate(current);
            final String currentString = (isValid ? current : getArgumentHelp()) + ' ';

            permissionBubbling = Stream.concat(permissionBubbling.stream(), getMeta().getPermissions().stream()).collect(Collectors.toSet());

            if (isValid || current == null) {
                Map<String, CommandArgumentMeta> completions = new LinkedHashMap<>();
                if (rest.isEmpty() && execution != null && meta.isSenderAllowed(sender.getClass())) {
                    completions.put(currentString, getMeta().withPermissions(permissionBubbling));
                }
                for (CommandArgument<?> child : children) {
                    for (Map.Entry<String, CommandArgumentMeta> completion : child.getPossiblePathsInternal(sender, rest, permissionBubbling).entrySet()) {
                        completions.put(currentString + completion.getKey(), completion.getValue());
                    }
                }
                return completions;
            }
        }
        return Collections.emptyMap();
    }

    public final Map<String, CommandArgumentMeta> getPossiblePaths(CommandSender sender, List<String> arguments) {
        return getPossiblePathsInternal(sender, arguments, new HashSet<>());
    }
}
