package com.emirilda.spigotmc.emirilda.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.Map;

public interface CommandExecution {
    boolean execute(CommandSender sender, Command command, Map<String, Object> namedParameters);
}
