package com.emirilda.spigotmc.emirilda.commands.arguments;

import com.emirilda.spigotmc.emirilda.commands.CommandArgument;

import java.util.Collections;
import java.util.List;

public class StringCommandArgument extends CommandArgument<String> {

    public StringCommandArgument(String argumentName) {
        super(argumentName, "string");
    }

    @Override
    public List<String> getTabCompletions() {
        return Collections.emptyList();
    }

    @Override
    public boolean validate(String argument) {
        return true;
    }

    @Override
    public String convertToType(String argument) {
        return argument;
    }

}
