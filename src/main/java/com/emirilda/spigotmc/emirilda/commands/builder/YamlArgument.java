package com.emirilda.spigotmc.emirilda.commands.builder;

import com.emirilda.spigotmc.emirilda.commands.CommandArgumentMeta;
import com.emirilda.spigotmc.emirilda.commands.arguments.*;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import com.google.common.base.Strings;
import com.emirilda.spigotmc.emirilda.commands.CommandArgument;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

@Log
public class YamlArgument {

    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String description;
    @Getter
    @Setter
    private String permission;
    @Getter
    @Setter
    private String hook;
    @Getter
    @Setter
    private ArgumentType type;
    @Getter
    @Setter
    private HashMap<String, YamlArgument> args;
    private boolean addedToHandler = false;
    private CommandArgument argument;

    public YamlArgument(String name, String description, String permission, String hook, ArgumentType type, HashMap<String, YamlArgument> args){

        this.name = name;
        this.description = description;
        this.permission = permission;
        this.hook = hook;
        this.type = type;
        this.args = args;

    }

    public CommandArgument addToHandler(CommandArgument parent, CommandBuilder builder){

        CommandArgument argument;

        CommandArgumentMeta meta = new CommandArgumentMeta();

        if(!Strings.isNullOrEmpty(description))
            meta.setDescription(description);

        if(!Strings.isNullOrEmpty(permission))
            meta.setPermissions(Collections.singleton(permission));

        if(!Strings.isNullOrEmpty(hook))
            meta.setCommandHook(hook);

        switch(type){
            case DOUBLE -> {

                argument = new DoubleCommandArgument(name);
                argument.withMeta(meta);

                if(!Strings.isNullOrEmpty(hook) && builder.getCommandHooks().containsKey(hook)){

                    argument.executes((sender, command, namedParameters) -> {

                        try {
                            builder.getCommandHooks().get(hook).invoke(builder.getCommandHookObjects().get(hook), sender, namedParameters.get("double"));
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            MessageUtility.logException(e, this.getClass(), log);
                        }

                        return true;
                    });

                }

            }
            case BOOLEAN -> {

                argument = new BooleanCommandArgument(name);
                argument.withMeta(meta);

                if(!Strings.isNullOrEmpty(hook) && builder.getCommandHooks().containsKey(hook)){

                    argument.executes((sender, command, namedParameters) -> {

                        try {
                            builder.getCommandHooks().get(hook).invoke(builder.getCommandHookObjects().get(hook), sender, namedParameters.get("boolean"));
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            MessageUtility.logException(e, this.getClass(), log);
                        }

                        return true;
                    });

                }

            }
            case INTEGER -> {

                argument = new IntegerCommandArgument(name);
                argument.withMeta(meta);

                if(!Strings.isNullOrEmpty(hook) && builder.getCommandHooks().containsKey(hook)){

                    argument.executes((sender, command, namedParameters) -> {

                        try {
                            builder.getCommandHooks().get(hook).invoke(builder.getCommandHookObjects().get(hook), sender, namedParameters.get("integer"));
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            MessageUtility.logException(e, this.getClass(), log);
                        }

                        return true;
                    });

                }

            }
            case LITERAL -> {

                argument = new LiteralCommandArgument(name);
                argument.withMeta(meta);

                if(!Strings.isNullOrEmpty(hook) && builder.getCommandHooks().containsKey(hook)){

                    argument.executes((sender, command, namedParameters) -> {

                        try {
                            builder.getCommandHooks().get(hook).invoke(builder.getCommandHookObjects().get(hook), sender);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            MessageUtility.logException(e, this.getClass(), log);
                        }

                        return true;
                    });

                }

            }
            case OFFPLAYER -> {

                argument = new OfflinePlayerCommandArgument(name);
                argument.withMeta(meta);

                if(!Strings.isNullOrEmpty(hook) && builder.getCommandHooks().containsKey(hook)){

                    argument.executes((sender, command, namedParameters) -> {

                        try {
                            builder.getCommandHooks().get(hook).invoke(builder.getCommandHookObjects().get(hook), sender, namedParameters.get("player"));
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }

                        return true;
                    });

                }

            }
            case ONPLAYER -> {

                argument = new OnlinePlayersCommandArgument(name);
                argument.withMeta(meta);

                if(!Strings.isNullOrEmpty(hook) && builder.getCommandHooks().containsKey(hook)){

                    argument.executes((sender, command, namedParameters) -> {

                        try {
                            builder.getCommandHooks().get(hook).invoke(builder.getCommandHookObjects().get(hook), sender, namedParameters.get("player"));
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            MessageUtility.logException(e, this.getClass(), log);
                        }

                        return true;
                    });

                }

            }
            case STRING -> {

                argument = new StringCommandArgument(name);
                argument.withMeta(meta);

                if(!Strings.isNullOrEmpty(hook) && builder.getCommandHooks().containsKey(hook)){

                    argument.executes((sender, command, namedParameters) -> {

                        try {
                            builder.getCommandHooks().get(hook).invoke(builder.getCommandHookObjects().get(hook), sender, namedParameters.get("string"));
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            MessageUtility.logException(e, this.getClass(), log);
                        }

                        return true;
                    });

                }

            }
            case HELP -> {

                argument = new LiteralCommandArgument(name);
                argument.withMeta(meta);
                argument.executes((sender, command, namedParameters) -> {
                    List<String> paths = new ArrayList<>();
                    paths.addAll((Collection) namedParameters.get("auto:path"));
                    paths.remove(0);
                    if(paths.get(0).equalsIgnoreCase("help"))
                        paths.remove(0);
                    namedParameters.put("auto:overflow", paths);
                    builder.getHandler().sendHelp(sender, command, namedParameters);
                    return true;
                });

            }
            default -> throw new IllegalStateException("Unexpected value: " + type);
        }

        parent.then(argument);

        addedToHandler = true;
        this.argument = argument;
        return parent;
    }

    public void addChildrenToHandler(CommandBuilder builder){
        if(addedToHandler)
            args.forEach((s, yamlArgument) -> {
                if(!yamlArgument.addedToHandler){
                    yamlArgument.addToHandler(argument, builder);
                    yamlArgument.addChildrenToHandler(builder);
                }
            });
    }

    public static YamlArgument loadFromYaml(YamlConfiguration yaml, String path, String name){

        String description = "";
        String permission = "";
        String hook = "";
        ArgumentType type = ArgumentType.LITERAL;
        HashMap<String, YamlArgument> args = new HashMap<>();

        if(yaml.contains(path+".description"))
            description = yaml.getString(path+".description");

        if(yaml.contains(path+".permission"))
            permission = yaml.getString(path+".permission");

        if(yaml.contains(path+".hook"))
            hook = yaml.getString(path+".hook");

        if(yaml.contains(path+".type"))
            type = ArgumentType.valueOf(Objects.requireNonNull(yaml.getString(path + ".type")).toUpperCase());

        if(yaml.contains(path+".args")){

            MemorySection section = (MemorySection) yaml.get(path+".args");
            assert section != null;
            Set<String> argPaths = section.getKeys(false);

            argPaths.forEach(argPath -> {
                args.put(argPath, YamlArgument.loadFromYaml(yaml, path+".args."+argPath, argPath));
            });

        }

        return new YamlArgument(name, description, permission, hook, type, args);

    }

    public enum ArgumentType {
        BOOLEAN, DOUBLE, INTEGER, LITERAL, OFFPLAYER, ONPLAYER, STRING, HELP;
    }

}
