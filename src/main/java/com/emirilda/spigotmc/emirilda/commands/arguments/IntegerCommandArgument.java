package com.emirilda.spigotmc.emirilda.commands.arguments;

import com.emirilda.spigotmc.emirilda.commands.CommandArgument;

import java.util.ArrayList;
import java.util.List;

public class IntegerCommandArgument extends CommandArgument<Integer> {

    public IntegerCommandArgument(String argumentName) {
        super(argumentName, "integer");
    }

    @Override
    public List<String> getTabCompletions() {
        return new ArrayList<>();
    }

    @Override
    public boolean validate(String argument) {
        if (argument.startsWith("-")) {
            argument = argument.substring(1);
        }
        for (char c : argument.toCharArray()) {
            if (!Character.isDigit(c))
                return false;
        }
        return true;
    }

    @Override
    public Integer convertToType(String argument) {
        return Integer.valueOf(argument);
    }

}
