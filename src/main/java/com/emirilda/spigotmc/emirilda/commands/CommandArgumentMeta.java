package com.emirilda.spigotmc.emirilda.commands;

import com.google.common.base.Splitter;
import lombok.*;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommandArgumentMeta {
    @Getter
    @Setter
    @With
    @Builder.Default
    private String description = "";
    @Getter
    @Setter
    @With
    @Builder.Default
    private String commandHook = "";
    @Getter
    @Setter
    @With
    @Builder.Default
    private Set<String> permissions = Collections.emptySet();
    @Getter
    @Setter
    @With
    @Builder.Default
    private EnumSet<AllowedCommandSender> allowedSenders = AllowedCommandSender.ALL;


    public final boolean isSenderAllowed(Class<? extends CommandSender> senderClass) {
        return allowedSenders.stream().anyMatch(s -> s.isSenderAllowed(senderClass));
    }

    public final boolean isSenderPermitted(CommandSender sender) {
        return permissions.size() == 0 || permissions.stream().anyMatch(sender::hasPermission);
    }

    public final boolean isSenderValid(CommandSender sender) {
        return isSenderAllowed(sender.getClass()) && isSenderPermitted(sender);
    }


    private static Splitter splitter = Splitter.on('|')
            .trimResults()
            .omitEmptyStrings();

    public static Set<String> decodePermissionString(String permissionString) {
        return StreamSupport.stream(splitter.split(permissionString).spliterator(), false).collect(Collectors.toSet());
    }
}
