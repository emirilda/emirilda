package com.emirilda.spigotmc.emirilda.commands.arguments;

import com.emirilda.spigotmc.emirilda.utility.MapUtility;
import com.emirilda.spigotmc.emirilda.commands.CommandArgument;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EnumCommandArgument<TEnum extends Enum<TEnum>> extends CommandArgument<TEnum> {
    private final Class<TEnum> enumClass;
    private final Map<String, TEnum> values;
    private final String argumentHelp;

    public EnumCommandArgument(String argumentName, Class<TEnum> enumClass, String argumentDisplayType) {
        super(argumentName);
        this.enumClass = enumClass;
        values = getValues(this.enumClass);
        if (argumentDisplayType != null) {
            this.argumentDisplayType = argumentDisplayType;
            this.argumentHelp = null;
        } else {
            this.argumentHelp = values.keySet().stream().sorted().collect(Collectors.joining(" | "));
        }
    }
    public EnumCommandArgument(String argumentName, Class<TEnum> enumClass) {
        this(argumentName, enumClass, null);
    }

    @Override
    public List<String> getTabCompletions() {
        return new ArrayList<>(values.keySet());
    }

    @Override
    public boolean validate(String argument) {
        return values.containsKey(argument.toLowerCase());
    }

    @Override
    public TEnum convertToType(String argument) {
        return values.get(argument.toLowerCase());
    }

    @Override
    public String getArgumentHelp() {
        if (argumentHelp != null)
            return argumentHelp;
        return super.getArgumentHelp();
    }

    private Map<String, TEnum> getValues(Class<TEnum> enumClass) {
        return MapUtility.hashMapFromEntries(Arrays.stream(enumClass.getEnumConstants()).map(e -> MapUtility.entry(e.name().toLowerCase().replace('_', '-'), e)).collect(Collectors.toList()));
    }
}
