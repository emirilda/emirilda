package com.emirilda.spigotmc.emirilda.commands.builder;

import com.emirilda.spigotmc.emirilda.commands.CommandArgumentMeta;
import com.emirilda.spigotmc.emirilda.commands.CommandHandler;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import com.google.common.base.Strings;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

@Log
public class YamlCommand {

    /*
        command:
            hook: test
            description: test
            permission: test
            args:
                arg1:
                    hook: test
                    description: test
                    permission: test
                    args: test
     */

    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String description;
    @Getter
    @Setter
    private String permission;
    @Getter
    @Setter
    private String hook;
    @Getter
    @Setter
    private HashMap<String, YamlArgument> args;

    public YamlCommand(String name, String description, String permission, String hook, HashMap<String, YamlArgument> args) {

        this.name = name;
        this.description = description;
        this.permission = permission;
        this.hook = hook;
        this.args = args;

    }

    public boolean addToHandler(CommandHandler handler, CommandBuilder builder) {

        CommandArgumentMeta meta = new CommandArgumentMeta();

        if (!Strings.isNullOrEmpty(description))
            meta.withDescription(description);

        if (!Strings.isNullOrEmpty(permission))
            meta.withPermissions(Collections.singleton(permission));

        if (!Strings.isNullOrEmpty(hook))
            meta.withCommandHook(hook);

        handler.addCommand(name).withMeta(meta);

        if (!Strings.isNullOrEmpty(hook) && builder.getCommandHooks().containsKey(hook)) {

            handler.getCommand(name).executes((sender, command, namedParameters) -> {

                try {
                    builder.getCommandHooks().get(hook).invoke(builder.getCommandHookObjects().get(hook), sender);
                } catch (IllegalAccessException e) {
                    MessageUtility.logException(e, this.getClass(), log);
                } catch (InvocationTargetException e) {
                    MessageUtility.logException(e, this.getClass(), log);
                }

                return true;
            });

        }
        return true;
    }

    public static YamlCommand loadFromYaml(YamlConfiguration yaml, String path) {

        String name = "";
        String description = "";
        String permission = "";
        String hook = "";
        HashMap<String, YamlArgument> args = new HashMap<>();

        name = path;

        if (yaml.contains(path + ".description"))
            description = yaml.getString(path + ".description");

        if (yaml.contains(path + ".permission"))
            permission = yaml.getString(path + ".permission");

        if (yaml.contains(path + ".hook"))
            hook = yaml.getString(path + ".hook");

        if (yaml.contains(path + ".args")) {

            MemorySection section = (MemorySection) yaml.get(path + ".args");
            Set<String> argPaths = section.getKeys(false);

            argPaths.forEach(argPath -> {
                args.put(argPath, YamlArgument.loadFromYaml(yaml, path + ".args." + argPath, argPath));
            });


        }

        return new YamlCommand(name, description, permission, hook, args);

    }

}
