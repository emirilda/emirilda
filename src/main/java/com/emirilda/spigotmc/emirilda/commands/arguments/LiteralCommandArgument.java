package com.emirilda.spigotmc.emirilda.commands.arguments;

import com.emirilda.spigotmc.emirilda.commands.CommandArgument;

import java.util.Collections;
import java.util.List;

public class LiteralCommandArgument extends CommandArgument<String> {

    private String label;

    public LiteralCommandArgument(String argumentName, String label) {
        super(argumentName);
        this.label = label;
    }

    public LiteralCommandArgument(String label) {
        this(label, label);
    }

    @Override
    public List<String> getTabCompletions() {
        return Collections.singletonList(label);
    }

    @Override
    public boolean validate(String argument) {
        return argument.equalsIgnoreCase(label);
    }

    @Override
    public String convertToType(String argument) {
        return label;
    }

    @Override
    public String getArgumentHelp() {
        return label;
    }

}
