package com.emirilda.spigotmc.emirilda.commands.arguments;

import com.emirilda.spigotmc.emirilda.commands.CommandArgument;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class OnlinePlayersCommandArgument extends CommandArgument<List<Player>> {

    private boolean allPlayers;

    public OnlinePlayersCommandArgument(String argumentName, boolean allPlayers) {
        super(argumentName, "player");
        this.allPlayers = allPlayers;
    }

    public OnlinePlayersCommandArgument(String argumentName) {
        this(argumentName, false);
    }

    @Override
    public List<String> getTabCompletions() {
        var players = Bukkit.getOnlinePlayers().stream();
        var output = players.map(HumanEntity::getName).collect(Collectors.toList());
        if(allPlayers) output.add("*");
        return output;
    }

    @Override
    public boolean validate(String argument) {
        if (argument.equals("*") && allPlayers)
            return true;
        var player = Bukkit.getPlayer(argument);
        return player != null;
    }

    @Override
    public List<Player> convertToType(String argument) {
        if (argument.equals("*")) {
            var players = Bukkit.getOnlinePlayers();
            return new ArrayList<>(players);
        }
        return Collections.singletonList(Bukkit.getPlayer(argument));
    }

    @Override
    public String getArgumentHelp() {
        return super.getArgumentHelp();
    }

}
