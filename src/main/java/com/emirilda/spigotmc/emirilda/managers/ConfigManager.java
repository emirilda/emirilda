package com.emirilda.spigotmc.emirilda.managers;

import com.emirilda.spigotmc.emirilda.utility.interfaces.Config;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class ConfigManager {

    private final Plugin plugin;
    private HashMap<String, Config> configs = new HashMap<>();

    public ConfigManager (Plugin plugin) {
        this.plugin = plugin;
    }

    public Config getConfig (String name) {
        if (!configs.containsKey((name+plugin.getDescription().getName()))) { configs.put(name+plugin.getDescription().getName(), new Config(plugin, name, Config.ConfigType.CONFIG)); }
        return configs.get(name+plugin.getDescription().getName());
    }
}