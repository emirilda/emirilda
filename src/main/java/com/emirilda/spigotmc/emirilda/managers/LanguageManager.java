package com.emirilda.spigotmc.emirilda.managers;

import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import com.emirilda.spigotmc.emirilda.utility.interfaces.Config;
import lombok.extern.java.Log;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

@Log
public class LanguageManager {

    private Plugin plugin;

    private Config languageFile;
    private Config config;

    public LanguageManager (Plugin plugin, Config config) {
        try {
            this.plugin = plugin;
            setUp(config);
        } catch (Exception ex) { MessageUtility.logException(ex, getClass(), log); }
    }

    public String getString (String path) {
        try {
            String message = (String) languageFile.get(path);

            if (message.length() <= 0) {
                message = ChatColor.RED + " Path \"" + path + "\" not found. Contact a server administrator about this issue. ";
            }
            return message;
        } catch (Exception e) { MessageUtility.logException(e, getClass(), log); }
        return null;
    }

    private void setUp (Config config) {
        try {
            this.config = config;
            new Config(plugin, "languages/english.yml", Config.ConfigType.LANG).copyFromResource("languages");

            try {
                languageFile = new Config(plugin, "languages/" + config.get("language") + ".yml", Config.ConfigType.LANG);
            } catch (Exception e) {
                plugin.getLogger().warning("Could not find language file" + config.get("language") + ".yml, make sure it exists!");
                languageFile = new Config(plugin, "languages/english.yml", Config.ConfigType.LANG);
                MessageUtility.logException(e, getClass(), log);
            }
        } catch (Exception ex) { MessageUtility.logException(ex, getClass(), log); }
    }

    public void reload () {
        setUp(config);
    }

}
