package com.emirilda.spigotmc.emirilda;

import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import com.emirilda.spigotmc.emirilda.utility.inventorygui.GuiEvents;
import com.emirilda.spigotmc.emirilda.datablocks.DataBlockChunk;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public final class Emirilda extends JavaPlugin {

    @Getter
    private static String pluginName, pluginVersion;
    /**
     * @apiNote Get an instance of Emirilda
     */
    @Getter
    private static Emirilda instance;

    @Getter
    @Setter
    private static boolean debug = false;
    @Getter
    private static boolean usingPlaceholderAPI = false;
    @Getter
    private static boolean usingTowny = false, usingWorldGuard = false;

    @Getter
    private static ConsoleCommandSender consoleCommandSender;

    @Getter
    private static HashMap<String, DataBlockChunk> dataBlocks;

    @Override
    public void onEnable() {
        /* Initializing information and classes */
        MessageUtility.logInfo(this, "Initializing classes and information...");
        pluginName = getDescription().getName();
        pluginVersion = getDescription().getVersion();
        instance = this;
        consoleCommandSender = getServer().getConsoleSender();
        dataBlocks = new HashMap<>();


        /* Checking for installed dependencies / apis */
        dependencyChecks();


        /* Registering events */
        getServer().getPluginManager().registerEvents(new GuiEvents(), this);

        MessageUtility.logInfo(this, "&fSuccessfully enabled " + pluginName + " v" + pluginVersion + "!");
    }

    @Override
    public void onDisable() {

    }

    @Deprecated
    public boolean registerNewInstance(Plugin ignoredPlugin) {
        /*pluginName = plugin.getDescription().getName();
        pluginVersion = plugin.getDescription().getVersion();*/
        return true;
    }


    private void dependencyChecks() {

        /* Checking if PlaceholderAPI is installed */
        if (getServer().getPluginManager().getPlugin("PlaceholderAPI") != null) {
            clog("PlaceholderAPI found, using it!");
            usingPlaceholderAPI = true;
        }


        /* Checking if Towny is installed */
        if (getServer().getPluginManager().getPlugin("Towny") != null) {
            clog("Towny found, using it!");
            usingTowny = true;
        }

        /* Checking if WorldGuard is installed */
        if (getServer().getPluginManager().getPlugin("WorldGuard") != null) {
            clog("WorldGuard found, using it!");
            usingWorldGuard = true;
        }

    }

    private void clog(String text) {
        getLogger().info("\u001b[37m" + text);
    }

}
