package com.emirilda.spigotmc.emirilda.utility;

import java.time.Duration;
import java.util.Arrays;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TextUtility {

    /**
     * Joins strings together in a readable fashion
     * <p>
     * For example; readableJoin("one", "two", "three", "four", "here we go")
     * would return; one, two, three, four and here we go
     *
     * @param strings "one", "two", "etc"
     * @return String
     */
    public static String readableJoin(String... strings) {
        if (strings == null || strings.length == 0)
            return "";
        if (strings.length == 1)
            return strings[0];
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < strings.length; i++) {
            output.append(strings[i]);
            if (i == strings.length - 2)
                output.append(" and ");
            else if (i < strings.length - 2)
                output.append(", ");
        }
        return output.toString();
    }

    /**
     * Joins strings together in a readable fashion with a customizable "and"
     * <p>
     * For example; readableJoin("och", "one", "two", "three", "four", "here we go")
     * would return; one, two, three, four och here we go
     *
     * @param and     ".. and .."
     * @param strings "one", "two", "etc"
     * @return String
     */
    public static String readableJoin(String and, String... strings) {
        if (strings == null || strings.length == 0)
            return "";
        if (strings.length == 1)
            return strings[0];
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < strings.length; i++) {
            output.append(strings[i]);
            if (i == strings.length - 2)
                output.append(" " + and + " ");
            else if (i < strings.length - 2)
                output.append(", ");
        }
        return output.toString();
    }

    /**
     * Makes things plural (in english)
     * Book -> Books
     * Cat -> Cats
     * Knife -> Knives
     * etc
     *
     * @param singular The word in singular
     * @return String
     */
    public static String pluralize(String singular) {
        if (singular == null || singular.isEmpty())
            return singular;

        String base = singular.substring(0, singular.length() - 1);
        if (singular.endsWith("y") && (singular.length() == 1 || !isVowel(singular.charAt(singular.length() - 2)))) // -(consonant)y -> -ies
            return base + "ies";
        if (singular.endsWith("f")) // -f -> -ves
            return base + "ves";

        base = singular.substring(0, singular.length() - 2);
        if (singular.endsWith("fe")) // -fe -> -ves
            return base + "ves";
        if (singular.endsWith("is")) // -is -> -es
            return base + "es";
        if (singular.endsWith("on")) // -on -> -a
            return base + "a";
        if (Stream.of("s", "x", "z", "o", "sh", "ch").anyMatch(singular::endsWith)) // sibilant or -o -> +es
            return singular + "es";

        return singular + "s"; // default: +s
    }

    /**
     * Simply checks if the given char is a vowel
     *
     * @return boolean
     */
    public static boolean isVowel(char c) {
        return "aoueiy".contains(Character.valueOf(c).toString());
    }


    /**
     * Takes time in seconds and returns a human-readable string.
     * For example; 785000 seconds with a maxUnits of 2 would become "9d 2h"
     * while 785000 seconds with a maxUnits of 4 would become "9d 2h 3m 20s"
     *
     * @param seconds  Amount of time in seconds
     * @param maxUnits How many units to return
     * @return String
     */
    public static String serializeDuration(long seconds, int maxUnits) {
        if (seconds < 1)
            return "0s";
        if (maxUnits < 1)
            maxUnits = Integer.MAX_VALUE;

        //@formatter:off
        var minutes = seconds / 60; seconds %= 60;
        var hours = minutes / 60; minutes %= 60;
        var days = hours / 24; hours %= 24;
        var years = days / 365; days %= 365;
        var months = days / 30; days %= 30;

        var strings = new Stack<String>();
        if (seconds != 0) strings.push(seconds + "s" );
        if (minutes != 0) strings.push(minutes + "m" );
        if (hours   != 0) strings.push(hours   + "h" );
        if (days    != 0) strings.push(days    + "d" );
        if (months  != 0) strings.push(months  + "mo");
        if (years   != 0) strings.push(years   + "y" );
        //@formatter:on

        int i = 0;
        var output = new StringBuilder();
        while (strings.size() > 0 && ++i <= maxUnits) {
            output.append(strings.pop()).append(' ');
        }
        return output.toString().trim();
    }

    /**
     * Takes a duration instead of seconds.
     * See {@link #serializeDuration(long, int) serializeDuration(long, int)}
     *
     * @return String
     */
    public static String serializeDuration(Duration duration, int maxUnits) {
        return serializeDuration(duration.getSeconds(), maxUnits);
    }

    /**
     * Capitalizes The String Fully (each word starts with a capital letter)
     *
     * @param input Your input string
     * @return String
     */
    public static String capitalizeFully(String input) {
        return Arrays.stream(input.split("\\s+")).map(string -> string.substring(0, 1).toUpperCase() +
                string.substring(1).toLowerCase()).collect(Collectors.joining(" "));
    }

}
