package com.emirilda.spigotmc.emirilda.utility;

import com.google.common.base.Preconditions;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

public class ChunkLocation implements Cloneable {
    private Reference<World> world;
    @Getter
    @Setter
    private int x, z;

    public static ChunkLocation of(Chunk chunk) {
        return new ChunkLocation(chunk.getWorld(), chunk.getX(), chunk.getZ());
    }

    public static ChunkLocation of(Location location) {
        return of(location.getChunk());
    }

    public ChunkLocation(@Nullable final World world, final int x, final int z) {
        setWorld(world);
        setX(x);
        setZ(z);
    }

    @Nullable
    public World getWorld() {
        if (this.world == null) {
            return null;
        }

        World world = this.world.get();
        Preconditions.checkArgument(world != null, "World unloaded");
        return world;
    }

    public void setWorld(@Nullable World world) {
        this.world = (world == null) ? null : new WeakReference<>(world);
    }

    public boolean isWorldLoaded() {
        if (this.world == null) {
            return false;
        }

        World world = this.world.get();
        return world != null && Bukkit.getWorld(world.getUID()) != null;
    }

    public Chunk getChunk() {
        World world = getWorld();
        if (world == null)
            return null;
        return world.getChunkAt(getX(), getZ());
    }

    @Override
    public String toString() {
        World world = (this.world == null) ? null : this.world.get();
        return "ChunkLocation{" +
                "world=" + world +
                ",x=" + x +
                ",z=" + z +
                '}';
    }

    public Vector toVector() {
        return new Vector(x, 0, z);
    }

    @Override
    public ChunkLocation clone() {
        try {
            return (ChunkLocation) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }
}