package com.emirilda.spigotmc.emirilda.utility;

import java.util.*;

public class MapUtility {

    public static <K, V> Map.Entry<K, V> entry(K key, V value) {
        return new AbstractMap.SimpleEntry<>(key, value);
    }

    public static <K, V> HashMap<K, V> hashMapFromEntries(Collection<Map.Entry<K, V>> entries) {
        var map = new HashMap<K, V>();
        for (var entry : entries) {
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }

    @SafeVarargs
    public static <K, V> HashMap<K, V> hashMapFromEntries(Map.Entry<K, V>... entries) {
        return hashMapFromEntries(Arrays.asList(entries));
    }

    public static <K, V> LinkedHashMap<K, V> linkedHashMapFromEntries(Collection<Map.Entry<K, V>> entries) {
        var map = new LinkedHashMap<K, V>();
        for (var entry : entries) {
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }

    @SafeVarargs
    public static <K, V> LinkedHashMap<K, V> linkedHashMapFromEntries(Map.Entry<K, V>... entries) {
        return linkedHashMapFromEntries(Arrays.asList(entries));
    }


}
