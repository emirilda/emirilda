package com.emirilda.spigotmc.emirilda.utility;

import com.emirilda.spigotmc.emirilda.Emirilda;
import com.palmergames.bukkit.towny.object.TownyPermission;
import com.palmergames.bukkit.towny.utils.PlayerCacheUtil;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class ProtectionUtility {

    public static boolean playerCanBuild(Player player, Block block) {
        boolean towny = true;
        boolean worldguard = true;

        if (Emirilda.isUsingTowny()) {
            towny = PlayerCacheUtil.getCachePermission(player, block.getLocation(), block.getType(), TownyPermission.ActionType.BUILD);
        }

        if (Emirilda.isUsingWorldGuard()) {
            LocalPlayer localPlayer = WorldGuardPlugin.inst().wrapPlayer(player);
            RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
            RegionQuery query = container.createQuery();

            boolean canBypass = WorldGuard.getInstance().getPlatform().getSessionManager().hasBypass(localPlayer, localPlayer.getWorld());

            worldguard = query.testState(BukkitAdapter.adapt(block.getLocation()), localPlayer, Flags.BUILD) || canBypass;

        }

        return towny && worldguard;
    }

}