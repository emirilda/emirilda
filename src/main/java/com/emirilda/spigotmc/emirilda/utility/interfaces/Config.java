package com.emirilda.spigotmc.emirilda.utility.interfaces;

import com.google.common.io.ByteStreams;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import lombok.Getter;
import lombok.extern.java.Log;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Objects;

@Log
public class Config {

    private String name;
    private File file;
    private YamlConfiguration config;
    private Plugin plugin;
    @Getter
    private ConfigType configType;

    public Config (Plugin plugin, String name, ConfigType configType) {
        this.plugin = plugin;
        this.name = name;
        this.configType = configType;
        file = Paths.get(plugin.getDataFolder().toString(), name).toFile();
        if (!file.exists()) {
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException exception) { MessageUtility.logException(exception, getClass(), log); }
        }
    }

    public Config save () {
        if ((this.config == null) || (this.file == null)) { return this; }
        try {
            if (config.getConfigurationSection("").getKeys(true).size() != 0) {
                config.save(this.file);
            }
        } catch (IOException exception) { MessageUtility.logException(exception, getClass(), log); }
        return this;
    }

    public YamlConfiguration get () {
        if (this.config == null) { reload(); }
        return this.config;
    }

    public Config saveDefaultConfig () {
        file = new File(plugin.getDataFolder(), this.name);
        plugin.saveResource(this.name, false);
        return this;
    }

    public Config clear () throws FileNotFoundException {
        new PrintWriter(file).close();
        return this;
    }

    public Config reload () {
        if (file == null) {
            this.file = new File(plugin.getDataFolder(), this.name);
        }
        this.config = YamlConfiguration.loadConfiguration(file);
        Reader defConfigStream;

        try {
            defConfigStream = new InputStreamReader(Objects.requireNonNull(plugin.getResource((configType == ConfigType.LANG ? "languages/" : "") + this.file.getName())), StandardCharsets.UTF_8);

            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            this.config.setDefaults(defConfig);
        } catch (NullPointerException ignored) { }
        return this;
    }

    public Config copyDefaults (boolean force) {
        get().options().copyDefaults(force);
        return this;
    }

    public Config copyFromResource () {
        if (file.length() <= 0) {
            try {
                try (InputStream in = plugin.getResource(this.file.getName());
                     OutputStream out = new FileOutputStream(this.file)) {
                    ByteStreams.copy(in, out);
                }
            } catch (Exception exception) { MessageUtility.logException(exception, getClass(), log); }
            try { config = YamlConfiguration.loadConfiguration(this.file); }
            catch (Exception exception) { MessageUtility.logException(exception, getClass(), log); }
        }
        return this;
    }

    public Config copyFromResource (String folder) {
        if (file.length() <= 0) {
            try {
                try (InputStream in = plugin.getResource(folder + "/" + this.file.getName());
                     OutputStream out = new FileOutputStream(this.file)) {
                    ByteStreams.copy(in, out);
                }
            } catch (Exception exception) {
                MessageUtility.logException(exception, getClass(), log);
            }
            try {
                config = YamlConfiguration.loadConfiguration(this.file);
            } catch (Exception exception) {
                MessageUtility.logException(exception, getClass(), log);
            }
        }
        return this;
    }

    public Config set (String key, Object value) {
        get().set(key, value);
        return this;
    }

    public Object get (String key) { return get().get(key); }

    public enum ConfigType {
        CONFIG,
        LANG
    }

}
