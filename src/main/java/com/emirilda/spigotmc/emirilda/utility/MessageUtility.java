package com.emirilda.spigotmc.emirilda.utility;

import com.emirilda.spigotmc.emirilda.Emirilda;
import lombok.extern.java.Log;
import me.clip.placeholderapi.PlaceholderAPI;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log
public class MessageUtility {

    /**
     * @deprecated in favor of {@link #colorizeMessage(Player, String...) colorizeMessage(Player, String...)}
     */
    @Deprecated
    public static String getColoredMessage(String... strings) {
        return colorizeMessage(strings);
    }

    /**
     * Converts color-codes into spigot colors.
     *
     * @param player  for PlaceholderAPI use, can be null
     * @param strings strings to color
     * @return String with spigot colors
     */
    public static String colorizeMessage(@Nullable Player player, String... strings) {

        if (Emirilda.isUsingPlaceholderAPI()) {
            return PlaceholderAPI.setPlaceholders(player, Objects.requireNonNull(colorizeMessage(strings)));
        }
        return colorizeMessage(strings);

    }

    private static String colorizeMessage(String... strings) {
        try {
            Pattern pattern = Pattern.compile("\\{#[a-fA-F\\d]{6}}");
            StringBuilder output = new StringBuilder();
            for (String string : strings) {
                Matcher matcher = pattern.matcher(string);
                while (matcher.find()) {
                    String hex = string.substring(matcher.start(), matcher.end());
                    string = string.replace(hex, ChatColor.of(hex.substring(1, 8)) + "");
                    matcher = pattern.matcher(string);
                }
                output.append(ChatColor.translateAlternateColorCodes('&', string));
            }
            return output.toString();
        } catch (Exception e) {
            logException(e, MessageUtility.class, log);
        }
        return null;
    }

    /**
     * @deprecated in favor of {@link #logException(Exception, Class, Logger) logException(Exception, Class, Logger)}
     */
    @Deprecated
    public static void logException(Exception exception, Class thrownClass) {
        boolean debug = Emirilda.isDebug();
        if (debug) {
            exception.printStackTrace();
        } else {
            Arrays.stream(exception.getStackTrace()).filter(exc -> {
                assert exc.getFileName() != null;
                return exc.getFileName().toLowerCase().contains(thrownClass.getSimpleName().toLowerCase());
            }).forEach(exc -> Emirilda.getConsoleCommandSender().sendMessage("[" + Emirilda.getPluginName() + "] " + org.bukkit.ChatColor.RED + String.format("Exception in %s %s on line %s caused by: %s", thrownClass.getPackage(), thrownClass.getSimpleName(), exc.getLineNumber(), exception.getMessage())));
        }
    }

    /**
     * Neat error logging
     *
     * @param exception   exception
     * @param thrownClass getClass()
     * @param logger      get this logger from Lombok by using @Log on your class
     */
    public static void logException(Exception exception, Class thrownClass, Logger logger) {
        boolean debug = Emirilda.isDebug();
        if (debug) {
            exception.printStackTrace();
        } else {
            Arrays.stream(exception.getStackTrace()).filter(exc -> {
                assert exc.getFileName() != null;
                return exc.getFileName().toLowerCase().contains(thrownClass.getSimpleName().toLowerCase());
            }).forEach(exc -> logger.warning(org.bukkit.ChatColor.RED + String.format("\nException on line %s caused by: %s", exc.getLineNumber(), exception.getMessage())));
        }
    }


    /**
     * Send info messages to console, for example on start-up
     *
     * @param plugin  The plugin you're working on (this)
     * @param message The message you want to send
     */
    public static void logInfo(Plugin plugin, String message) {
        plugin.getLogger().info("\u001b[37m" + message);
    }

}
