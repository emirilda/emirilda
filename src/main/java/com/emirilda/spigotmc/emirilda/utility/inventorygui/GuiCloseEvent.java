package com.emirilda.spigotmc.emirilda.utility.inventorygui;

import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GuiCloseEvent extends Event {

    private static final HandlerList HANDLERS = new HandlerList();

    @Getter
    private final Player player;

    public GuiCloseEvent(Player player){
        this.player = player;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }


}
