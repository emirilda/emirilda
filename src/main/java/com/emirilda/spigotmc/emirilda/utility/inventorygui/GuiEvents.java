package com.emirilda.spigotmc.emirilda.utility.inventorygui;

import com.emirilda.spigotmc.emirilda.Emirilda;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import lombok.extern.java.Log;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Objects;
import java.util.UUID;

@Log
public class GuiEvents implements Listener {

    /* Basic click and drag events, executes action if found */

    private void inventoryInteractEvent (InventoryInteractEvent rawEvent) {
        try {
            InventoryClickEvent event = (InventoryClickEvent) rawEvent;

            if (!(event.getWhoClicked() instanceof Player player)) return;

            UUID playerUUID = player.getUniqueId();
            UUID inventoryUUID = InventoryGui.getOpenInventories().get(playerUUID);

            if (inventoryUUID != null) {
                InventoryGui gui = InventoryGui.getInventoriesByUUID().get(inventoryUUID);
                if (gui.getType() == InventoryGui.GUIType.UNEDITABLE) { event.setCancelled(true); }
                InventoryGui.GUIAction action = gui.getActions().get(event.getSlot());
                if (action != null && Objects.requireNonNull(event.getClickedInventory()).getType() != InventoryType.PLAYER) { action.click(player, event); }
            }

        } catch (Exception exception) { MessageUtility.logException(exception, getClass(), log); }
    }

    @EventHandler
    public void onClick (InventoryClickEvent event) { inventoryInteractEvent(event); }

    @EventHandler
    public void onDragEvent (InventoryDragEvent event) {
        try {
            if (!(event.getWhoClicked() instanceof Player player)) return;
            UUID playerUUID = player.getUniqueId();
            UUID inventoryUUID = InventoryGui.getOpenInventories().get(playerUUID);
            if(inventoryUUID == null) return;

            if (event.getNewItems().containsKey(22)) {
                event.setCancelled(true);
                event.getView().getTopInventory().setItem(22, event.getNewItems().get(22));
                new BukkitRunnable(){
                    @Override
                    public void run () {
                        if(event.getType() == DragType.SINGLE){
                            ItemStack cursor = event.getWhoClicked().getOpenInventory().getCursor();
                            assert cursor != null;
                            cursor.setAmount(cursor.getAmount()-1);
                            event.getWhoClicked().getInventory().addItem(cursor);
                        }
                        event.getWhoClicked().getOpenInventory().setCursor(null);
                        cancel();
                    }
                }.runTaskTimer(Emirilda.getInstance(), 1, 0);
                inventoryInteractEvent(new InventoryClickEvent(event.getView(), InventoryType.SlotType.CONTAINER, 22, ClickType.LEFT, InventoryAction.PLACE_ALL));
            }
        } catch (Exception exception) { MessageUtility.logException(exception, getClass(), log); }
    }

    /* Events for removing the player from the openInventories list if they exit the inventory */
    @EventHandler
    public void onClose (InventoryCloseEvent event) {
        try {
            Player player = (Player) event.getPlayer();
            UUID uuid = player.getUniqueId();
            if (InventoryGui.getOpenInventories().containsKey(uuid)) {
                InventoryGui.getInventoriesByUUID().get(InventoryGui.getOpenInventories().get(uuid)).closeExecutions(player);
                InventoryGui.getOpenInventories().remove(uuid);
                GuiCloseEvent guiCloseEvent = new GuiCloseEvent(player);
                Emirilda.getInstance().getServer().getPluginManager().callEvent(guiCloseEvent);
            }
        } catch (Exception exception) { MessageUtility.logException(exception, getClass(), log); }
    }

    @EventHandler
    public void onQuit (PlayerQuitEvent event) {
        try {
            Player player = event.getPlayer();
            UUID uuid = player.getUniqueId();
            if (InventoryGui.getOpenInventories().containsKey(uuid)) {
                InventoryGui.getInventoriesByUUID().get(InventoryGui.getOpenInventories().get(uuid)).closeExecutions(player);
                InventoryGui.getOpenInventories().remove(uuid);
                GuiCloseEvent guiCloseEvent = new GuiCloseEvent(player);
                Emirilda.getInstance().getServer().getPluginManager().callEvent(guiCloseEvent);
            }
        } catch (Exception exception) { MessageUtility.logException(exception, getClass(), log); }
    }

}

