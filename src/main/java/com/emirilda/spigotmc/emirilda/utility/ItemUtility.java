package com.emirilda.spigotmc.emirilda.utility;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.List;

public class ItemUtility {

    /**
     *
     * Get an {@link ItemStack} with name and lore using an {@link ItemStack}
     *
     * @param item the {@link ItemStack} to use
     * @param name name to give item
     * @param lore lore to give item
     *
     * @return ItemStack
     */
    public static ItemStack createItem (ItemStack item, String name, List<String> lore) {
        ItemStack tempItem = item.clone();
        ItemMeta meta = tempItem.getItemMeta();
        assert meta != null;
        meta.setDisplayName(name);
        meta.setLore(lore);
        tempItem.setItemMeta(meta);
        return tempItem;
    }

    /**
     *
     * Get an {@link ItemStack} with name and lore using a {@link Material}
     *
     * @param item the {@link Material} to use
     * @param name name to give item
     * @param lore lore to give item
     *
     * @return ItemStack
     */
    public static ItemStack createItem (Material item, String name, List<String> lore) { return createItem(new ItemStack(item), name, lore); }

    /**
     *
     * Get an {@link ItemStack} with a name using an {@link ItemStack}
     *
     * @param item the {@link ItemStack} to use
     * @param name name to give item
     *
     * @return ItemStack
     */
    public static ItemStack createItem (ItemStack item, String name) {
        ItemStack tempItem = item.clone();
        ItemMeta meta = tempItem.getItemMeta();
        assert meta != null;
        meta.setDisplayName(name);
        tempItem.setItemMeta(meta);
        return tempItem;
    }

    /**
     *
     * Get leather armor with a name and a {@link Color} using an {@link Material}
     *
     * @param item the {@link Material} to use
     * @param name to give item
     * @param color {@link Color} of the armor
     *
     * @return ItemStack
     */
    public static ItemStack createItem (Material item, String name, Color color) { return createItem(new ItemStack(item), name, color); }

    /**
     *
     * Get leather armor with a name and a {@link Color} using an {@link ItemStack}
     *
     * @param item the {@link ItemStack} to use
     * @param name to give item
     * @param color {@link Color} of the armor
     *
     * @return ItemStack
     */
    public static ItemStack createItem (ItemStack item, String name, Color color) {
        LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
        assert meta != null;
        meta.setDisplayName(name);
        meta.setColor(color);
        item.setItemMeta(meta);
        return item;
    }


    /**
     *
     * Get an {@link ItemStack} with a name using a {@link Material}
     *
     * @param item the {@link Material} to use
     * @param name name to give item
     *
     * @return ItemStack
     */
    public static ItemStack createItem (Material item, String name) {
        return createItem(new ItemStack(item), name);
    }


    /**
     *
     * Bad method for getting the english name of an item. You probably shouldn't use this..
     *
     * @param material material to get name of
     *
     * @return String
     */
    public static String getItemName(Material material){

        return TextUtility.capitalizeFully(material.name().replace("_", " "));
    }

}
