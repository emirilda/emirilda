package com.emirilda.spigotmc.emirilda.utility;

import javax.annotation.Nullable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateTimeUtility {

    public static int compareDays(LocalDate first, LocalDate last) {
        return (int) (last.toEpochDay() - first.toEpochDay());
    }

    public static int compareDays(Instant first, Instant last) {
        return getEpochDay(last) - getEpochDay(first);
    }

    public static String serializeDateTime(@Nullable Instant instant, boolean alwaysFull) {
        if (instant == null) return "";
        if (!alwaysFull) {
            var days = compareDays(Instant.now(), instant);
            if (days == 0) {
                return String.format("today %s", format(instant, "HH:mm:ss"));
            } else if (days == -1) {
                return String.format("yesterday %s", format(instant, "HH:mm:ss"));
            }
        }
        return format(instant, "yyyy-MM-dd HH:mm:ss");
    }

    public static String serializeDate(@Nullable Instant instant, boolean alwaysFull) {
        if (instant == null) return "";
        if (!alwaysFull) {
            var days = compareDays(Instant.now(), instant);
            if (days == 0) {
                return "today";
            } else if (days == -1) {
                return "yesterday";
            }
        }
        return format(instant, "yyyy-MM-dd");
    }

    public static String serializeTime(@Nullable Instant instant) {
        if (instant == null) return "";
        return format(instant, "HH:mm:ss");
    }

    private static LocalDateTime instantToLDT(Instant instant) {
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }

    private static int getEpochDay(Instant instant) {
        return (int) instantToLDT(instant).toLocalDate().toEpochDay();
    }

    private static String format(Instant instant, String format) {
        return DateTimeFormatter.ofPattern(format).format(instantToLDT(instant));
    }


}
