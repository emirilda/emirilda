package com.emirilda.spigotmc.emirilda.datablocks.events;

import com.emirilda.spigotmc.emirilda.datablocks.DataBlockManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class BlockBreakEvent implements Listener {

    private final DataBlockManager dataBlockManager;

    public BlockBreakEvent(DataBlockManager dataBlockManager){
        this.dataBlockManager = dataBlockManager;
    }


    @EventHandler
    public void blockBreak(org.bukkit.event.block.BlockBreakEvent event){

        dataBlockManager.removeDataBlock(event.getBlock());

    }

}
