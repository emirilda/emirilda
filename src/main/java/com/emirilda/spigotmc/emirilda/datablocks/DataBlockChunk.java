package com.emirilda.spigotmc.emirilda.datablocks;

import lombok.Getter;

import java.util.HashMap;

@Getter
public class DataBlockChunk {

    private final HashMap<String, DataBlock> dataBlocks;

    public DataBlockChunk(){
        dataBlocks = new HashMap<>();
    }

}
