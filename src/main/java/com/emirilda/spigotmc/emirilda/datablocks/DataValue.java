package com.emirilda.spigotmc.emirilda.datablocks;

import lombok.Getter;

@Getter
public abstract class DataValue<TClass> {

    private final String key = "";
    private final TClass value;
    private final DataValueType type;

    public static DataValue convertFromString(String raw) {
        return null;
    }

    public abstract String convertToString();

    public DataValue(TClass value, DataValueType type){
        this.value = value;
        this.type = type;
    }



}
