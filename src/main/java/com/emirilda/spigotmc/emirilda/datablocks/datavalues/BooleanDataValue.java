package com.emirilda.spigotmc.emirilda.datablocks.datavalues;

import com.emirilda.spigotmc.emirilda.datablocks.DataValue;
import com.emirilda.spigotmc.emirilda.datablocks.DataValueType;

public class BooleanDataValue extends DataValue<Boolean> {

    public BooleanDataValue(Boolean value) {
        super(value, DataValueType.BOOLEAN);
    }

    public static BooleanDataValue convertFromString(String raw) {
        return new BooleanDataValue(Boolean.valueOf(raw));
    }

    @Override
    public String convertToString() {
        return Boolean.toString(getValue());
    }
}
