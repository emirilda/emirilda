package com.emirilda.spigotmc.emirilda.datablocks.events;

import com.emirilda.spigotmc.emirilda.datablocks.DataBlockManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PluginDisableEvent implements Listener {
    
    private final DataBlockManager dataBlockManager;
    
    public PluginDisableEvent(DataBlockManager dataBlockManager){
        this.dataBlockManager = dataBlockManager;
    }
    
    @EventHandler
    public void disableEvent(org.bukkit.event.server.PluginDisableEvent event){
        dataBlockManager.saveKnownDatachunks();
        dataBlockManager.setServerStopping(true);
        dataBlockManager.removeAllPendingDatablocks();
    }
    
}