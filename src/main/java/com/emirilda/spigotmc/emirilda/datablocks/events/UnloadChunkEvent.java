package com.emirilda.spigotmc.emirilda.datablocks.events;

import com.emirilda.spigotmc.emirilda.Emirilda;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import com.emirilda.spigotmc.emirilda.datablocks.DataBlockManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

public class UnloadChunkEvent implements Listener {

    private final DataBlockManager dataBlockManager;

    public UnloadChunkEvent(DataBlockManager dataBlockManager){
        this.dataBlockManager = dataBlockManager;
    }

    @EventHandler
    public void unloadEvent(ChunkUnloadEvent event){

        String chunkLocation = DatabaseSerializationUtility.serializeChunkLocation(event.getChunk());

        if(Emirilda.getDataBlocks().containsKey(chunkLocation)){
            dataBlockManager.saveChunkToDatabase(Emirilda.getDataBlocks().get(chunkLocation));
            Emirilda.getDataBlocks().remove(chunkLocation);
        }


    }

}
