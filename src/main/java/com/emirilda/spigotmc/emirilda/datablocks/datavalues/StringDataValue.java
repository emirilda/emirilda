package com.emirilda.spigotmc.emirilda.datablocks.datavalues;

import com.emirilda.spigotmc.emirilda.datablocks.DataValue;
import com.emirilda.spigotmc.emirilda.datablocks.DataValueType;

public class StringDataValue extends DataValue<String> {

    public StringDataValue(String value) {
        super(value, DataValueType.STRING);
    }

    @Override
    public String convertToString() {
        return getValue();
    }

    public static StringDataValue convertFromString(String raw) {
        return new StringDataValue(raw);
    }
}
