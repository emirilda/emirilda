package com.emirilda.spigotmc.emirilda.datablocks;

import com.emirilda.spigotmc.emirilda.datablocks.datavalues.BooleanDataValue;
import com.emirilda.spigotmc.emirilda.datablocks.datavalues.StringDataValue;
import lombok.Getter;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Getter
public class DataBlock {

    private Block block;
    private HashMap<String, DataValue> entries;
    private UUID uid;

    public DataBlock(Block block){
        init(block, new HashMap<>(), UUID.randomUUID());
    }

    public DataBlock(Block block, HashMap<String, DataValue> entries){
        init(block, entries, UUID.randomUUID());
    }

    public DataBlock(Block block, HashMap<String, DataValue> entries, UUID uid){
        init(block, entries, uid);
    }

    private void init(Block block, HashMap<String, DataValue> entries, UUID uid){
        this.block = block;
        this.entries = entries;
        this.uid = uid;
    }

    public String serializeToString(){
        YamlConfiguration yamlConfiguration = new YamlConfiguration();

        entries.forEach((string, dataValue) -> {
            yamlConfiguration.set("values."+string+".value", dataValue.convertToString());
            yamlConfiguration.set("values."+string+".type", dataValue.getType().toString());
        });

        return yamlConfiguration.saveToString();
    }

    public static DataBlock serializeFromString(String input, Block block, UUID uid){
        YamlConfiguration yamlConfiguration = new YamlConfiguration();
        try {
            yamlConfiguration.loadFromString(input);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        if(yamlConfiguration.contains("values")){

            MemorySection memorySection = (MemorySection) yamlConfiguration.get("values");
            List<String> paths = memorySection.getKeys(false).stream().toList();

            HashMap<String, DataValue> values = new HashMap<>();

            paths.forEach(path -> {
                DataValueType type = DataValueType.valueOf(yamlConfiguration.getString("values."+path+".type"));
                switch(type){
                    case STRING -> values.put(path, StringDataValue.convertFromString(yamlConfiguration.getString("values."+path+".value")));
                    case BOOLEAN -> values.put(path, BooleanDataValue.convertFromString(yamlConfiguration.getString("values."+path+".value")));

                }
            });

            return new DataBlock(block, values, uid);

        }else return new DataBlock(block, new HashMap<>(), uid);
    }

    public String getString(String path){
        return (String) entries.get(path).getValue();
    }

    public void setString(String path, String value){
        StringDataValue stringDataValue = new StringDataValue(value);
        entries.put(path, stringDataValue);
    }

    public Boolean getBoolean(String path){
        return (Boolean) entries.get(path).getValue();
    }

    public void setBoolean(String path, Boolean value){
        BooleanDataValue booleanDataValue = new BooleanDataValue(value);
        entries.put(path, booleanDataValue);
    }

    public boolean contains(String path){
        return entries.containsKey(path);
    }

    public boolean clear(){
        entries.clear();
        return true;
    }

}
