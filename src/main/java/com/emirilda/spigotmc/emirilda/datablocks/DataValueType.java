package com.emirilda.spigotmc.emirilda.datablocks;

public enum DataValueType {

    STRING, BOOLEAN;

}
