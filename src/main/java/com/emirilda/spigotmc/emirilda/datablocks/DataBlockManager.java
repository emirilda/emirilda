package com.emirilda.spigotmc.emirilda.datablocks;

import com.emirilda.spigotmc.emirilda.Emirilda;
import com.emirilda.spigotmc.emirilda.datablocks.events.BlockBreakEvent;
import com.emirilda.spigotmc.emirilda.datablocks.events.ChunkLoadEvent;
import com.emirilda.spigotmc.emirilda.datablocks.events.UnloadChunkEvent;
import com.emirilda.spigotmc.emirilda.database.DatabaseManager;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.bukkit.Chunk;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.emirilda.spigotmc.emirilda.database.DatabaseValues.*;

@Log
public class DataBlockManager {

    @Getter
    protected DatabaseManager databaseManager;

    @Getter
    protected List<String> dataChunks;

    private final String datablockTable = "TEMDLib_datablocks";
    private final String chunkTable = "TEMDLib_datablocks_chunks";

    private final List<Block> dataBlocksToRemove;

    @Setter
    private boolean serverStopping = false;

    private int x = 0;

    public DataBlockManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        this.dataChunks = new ArrayList<>();
        this.dataBlocksToRemove = new ArrayList<>();

        initDatabase();
        registerKnownDatachunks();
        Emirilda.getInstance().getServer().getPluginManager().registerEvents(new ChunkLoadEvent(this), Emirilda.getInstance());
        Emirilda.getInstance().getServer().getPluginManager().registerEvents(new UnloadChunkEvent(this), Emirilda.getInstance());
        Emirilda.getInstance().getServer().getPluginManager().registerEvents(new BlockBreakEvent(this), Emirilda.getInstance());

        removeDataBlocks();

    }


    private void initDatabase() {
        databaseManager.createTable(datablockTable,
                                    "`uid` " + VARCHAR.get() + PRIMARY_KEY.get() + "," +
                                    "`chunk_location` " + TINYTEXT.get() + "," +
                                    "`block_location` " + TINYTEXT.get() + "," +
                                    "`data` " + TEXT.get()
        );
        databaseManager.createTable(chunkTable,
                                    "`chunk_location` " + VARCHAR.get() + PRIMARY_KEY.get()
        );
    }

    public void removeAllPendingDatablocks() {
        if(!dataBlocksToRemove.isEmpty()){
            dataBlocksToRemove.forEach(this::removeDataBlockInternally);
        }
    }

    private void removeDataBlocks() {


        new BukkitRunnable() {
            @Override
            public void run() {
                if (dataBlocksToRemove.size() > 8)
                    for (int i = 0; i < 7; i++) {
                        Block dataBlock = dataBlocksToRemove.get(0);

                        removeDataBlockInternally(dataBlock);
                        dataBlocksToRemove.remove(0);
                    }
                else
                    for (int i = 0; i < dataBlocksToRemove.size(); i++) {
                        Block dataBlock = dataBlocksToRemove.get(0);

                        removeDataBlockInternally(dataBlock);
                        dataBlocksToRemove.remove(0);
                    }
                x++;

                if(x==300){
                    x=0;
                    log.info("Current datablocks pending: " + dataBlocksToRemove.size());
                }

                if (serverStopping) cancel();
            }
        }.runTaskTimerAsynchronously(Emirilda.getInstance(), 100, 2);

    }

    private void removeDataBlockInternally(Block block) {

        DataBlock dataBlock = getDataBlock(block);

        String blockLocation = DatabaseSerializationUtility.serializeLocation(dataBlock.getBlock().getLocation());
        String chunkLocation = DatabaseSerializationUtility.serializeChunkLocation(dataBlock.getBlock().getChunk());

        if (Emirilda.getDataBlocks().containsKey(chunkLocation)) {
            DataBlockChunk dataBlockChunk = Emirilda.getDataBlocks().get(chunkLocation);
            dataBlockChunk.getDataBlocks().remove(blockLocation);
            databaseManager.execute("DELETE FROM " + datablockTable + " WHERE `block_location`=\"" + blockLocation + "\";");
            if (dataBlockChunk.getDataBlocks().isEmpty()) {
                Emirilda.getDataBlocks().remove(chunkLocation);
                dataChunks.remove(chunkLocation);
            } else
                Emirilda.getDataBlocks().put(chunkLocation, dataBlockChunk);
        }
    }

    private void registerKnownDatachunks() {
        ResultSet result = databaseManager.executeQuery("SELECT * FROM `" + chunkTable + "`;");
        try {
            while (result.next()) {
                String chunkLocation = result.getString("chunk_location");
                dataChunks.add(chunkLocation);
            }
        } catch (SQLException e) {
            MessageUtility.logException(e, getClass(), log);
        }
    }

    public void saveKnownDatachunks() {
        try {
            databaseManager.execute("DELETE FROM `" + chunkTable + "`;");

            dataChunks.forEach(chunk ->
                    databaseManager.execute("INSERT INTO `" + chunkTable + "` (chunk_location) VALUES (\"" + chunk + "\");")
            );

        } catch (Exception e) {
            MessageUtility.logException(e, getClass(), log);
        }
    }

    public void saveDataBlockToDatabase(DataBlock dataBlock) {
        if (!dataBlock.getEntries().isEmpty())
            if (databaseManager.isSqlite()) {
                databaseManager.execute("INSERT OR REPLACE INTO `" + datablockTable + "` " +
                        "(uid, chunk_location, block_location, data) VALUES (" +
                        "\"" + dataBlock.getUid().toString() + "\"," +
                        "\"" + DatabaseSerializationUtility.serializeChunkLocation(dataBlock.getBlock().getChunk()) + "\"," +
                        "\"" + DatabaseSerializationUtility.serializeLocation(dataBlock.getBlock().getLocation()) + "\"," +
                        "\"" + dataBlock.serializeToString() + "\"); ");
            } else {
                databaseManager.execute("INSERT INTO `" + datablockTable + "` " +
                        "(uid, chunk_location, block_location, data) VALUES (" +
                        "\"" + dataBlock.getUid().toString() + "\"," +
                        "\"" + DatabaseSerializationUtility.serializeChunkLocation(dataBlock.getBlock().getChunk()) + "\"," +
                        "\"" + DatabaseSerializationUtility.serializeLocation(dataBlock.getBlock().getLocation()) + "\"," +
                        "\"" + dataBlock.serializeToString() + "\") " +
                        "ON DUPLICATE KEY UPDATE " +
                        "`data` = \"" + dataBlock.serializeToString() + "\";"
                );
            }
    }

    public void saveDataBlock(DataBlock dataBlock) {
        String chunkLocation = DatabaseSerializationUtility.serializeChunkLocation(dataBlock.getBlock().getChunk());
        String blockLocation = DatabaseSerializationUtility.serializeLocation(dataBlock.getBlock().getLocation());

        DataBlockChunk dataBlockChunk;
        if (Emirilda.getDataBlocks().containsKey(chunkLocation)) {
            dataBlockChunk = Emirilda.getDataBlocks().get(chunkLocation);
        } else {
            dataBlockChunk = new DataBlockChunk();
        }
        dataBlockChunk.getDataBlocks().put(blockLocation, dataBlock);
        Emirilda.getDataBlocks().put(chunkLocation, dataBlockChunk);

    }

    public DataBlockChunk loadChunk(Chunk chunk) {
        String chunkLocation = DatabaseSerializationUtility.serializeChunkLocation(chunk);
        DataBlockChunk dataBlockChunk;

        if (Emirilda.getDataBlocks().containsKey(chunkLocation))
            dataBlockChunk = Emirilda.getDataBlocks().get(chunkLocation);
        else
            dataBlockChunk = new DataBlockChunk();

        ResultSet result = databaseManager.executeQuery("SELECT * FROM `" + datablockTable + "` WHERE `chunk_location`=\"" +
                chunkLocation + "\";");
        try {
            while (result.next()) {

                UUID uid = UUID.fromString(result.getString("uid"));
                String blockLocation = result.getString("block_location");
                Block block = DatabaseSerializationUtility.deserializeLocation(blockLocation).getBlock();
                String rawInput = result.getString("data");

                DataBlock dataBlock = DataBlock.serializeFromString(rawInput, block, uid);

                dataBlockChunk.getDataBlocks().put(blockLocation, dataBlock);

            }
        } catch (SQLException e) {
            MessageUtility.logException(e, getClass(), log);
        }

        return dataBlockChunk;

    }

    public void saveAll() {
        Emirilda.getDataBlocks().forEach((s, dataBlockChunk) -> saveChunkToDatabase(dataBlockChunk));
    }

    public void saveChunkToDatabase(DataBlockChunk dataBlockChunk) {

        dataBlockChunk.getDataBlocks().forEach((key, value) -> saveDataBlockToDatabase(value));

    }

    public void removeDataBlock(Block dataBlock) {
        dataBlocksToRemove.add(dataBlock);
    }

    public DataBlock getDataBlock(Block block) {
        String chunkLocation = DatabaseSerializationUtility.serializeChunkLocation(block.getChunk());
        String blockLocation = DatabaseSerializationUtility.serializeLocation(block.getLocation());

        DataBlockChunk dataBlockChunk;
        if (Emirilda.getDataBlocks().containsKey(chunkLocation)) {
            dataBlockChunk = Emirilda.getDataBlocks().get(chunkLocation);
            if (!dataBlockChunk.getDataBlocks().containsKey(blockLocation)) {
                dataBlockChunk.getDataBlocks().put(blockLocation, getDataBlockFromDatabase(block));
                Emirilda.getDataBlocks().put(chunkLocation, dataBlockChunk);
            }
        } else {
            dataBlockChunk = new DataBlockChunk();
            dataBlockChunk.getDataBlocks().put(blockLocation, getDataBlockFromDatabase(block));
            Emirilda.getDataBlocks().put(chunkLocation, dataBlockChunk);
        }

        if (!dataChunks.contains(chunkLocation)) dataChunks.add(chunkLocation);

        return dataBlockChunk.getDataBlocks().get(blockLocation);
    }

    public boolean isDataBlock(Block block) {

        String chunkLocation = DatabaseSerializationUtility.serializeChunkLocation(block.getChunk());
        String blockLocation = DatabaseSerializationUtility.serializeLocation(block.getLocation());

        return (Emirilda.getDataBlocks().containsKey(chunkLocation) && Emirilda.getDataBlocks().get(chunkLocation).getDataBlocks().containsKey(blockLocation));

    }

    private DataBlock getDataBlockFromDatabase(Block block) {
        ResultSet result = databaseManager.executeQuery("SELECT * FROM `" + datablockTable + "` WHERE `block_location`=\"" +
                DatabaseSerializationUtility.serializeLocation(block.getLocation()) + "\";");
        try {
            if (result.getFetchSize() > 0) {
                UUID uid = UUID.fromString(result.getString("uid"));
                String rawInput = result.getString("data");

                return DataBlock.serializeFromString(rawInput, block, uid);

            } else {
                return new DataBlock(block);
            }
        } catch (SQLException e) {
            MessageUtility.logException(e, getClass(), log);
            return null;
        }
    }

}
