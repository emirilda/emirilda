package com.emirilda.spigotmc.emirilda.datablocks.events;

import com.emirilda.spigotmc.emirilda.Emirilda;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import com.emirilda.spigotmc.emirilda.datablocks.DataBlockChunk;
import com.emirilda.spigotmc.emirilda.datablocks.DataBlockManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class ChunkLoadEvent implements Listener {

    private final DataBlockManager dataBlockManager;

    public ChunkLoadEvent(DataBlockManager dataBlockManager){
        this.dataBlockManager = dataBlockManager;
    }

    @EventHandler
    public void loadEvent(org.bukkit.event.world.ChunkLoadEvent event){

        String chunkLocation = DatabaseSerializationUtility.serializeChunkLocation(event.getChunk());

        if(!dataBlockManager.getDataChunks().contains(chunkLocation) || Emirilda.getDataBlocks().containsKey(chunkLocation)) return;

        DataBlockChunk dataBlockChunk = dataBlockManager.loadChunk(event.getChunk());

        if(dataBlockChunk != null && !dataBlockChunk.getDataBlocks().isEmpty())
            Emirilda.getDataBlocks().put(chunkLocation, dataBlockChunk);

    }

}
