package com.emirilda.spigotmc.emirilda.database;

import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import lombok.Getter;
import lombok.extern.java.Log;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.sql.*;

@Log
public class DatabaseManager {

    /*
     * database:
     *   database: 'database'
     *   host: 'localhost'
     *   port: 3306
     *   username: 'username'
     *   password: 'password'
     */

    private Plugin plugin;

    private String database, host, username, password, filename;
    private int port;
    private Connection connection;
    private Statement statement;
    @Getter
    private boolean sqlite;

    public DatabaseManager(Plugin plugin, String database, String host, String username, String password, int port){
        this.plugin = plugin;
        try {
            this.database = database;
            this.host = host;
            this.username = username;
            this.password = password;
            this.port = port;
            this.sqlite = false;
        }catch(Exception e){
            MessageUtility.logException(e, getClass(), log);
        }
    }

    public DatabaseManager(Plugin plugin, String filename){
        this.plugin = plugin;
        try {
            if(filename.endsWith(".db"))
                this.filename = filename;
            else
                this.filename = filename+".db";

            this.sqlite = true;
        }catch(Exception e){
            MessageUtility.logException(e, getClass(), log);
        }
    }

    public void initializeConnection(){
        try {
            if(connection == null || connection.isClosed() || !connection.isValid(connection.getNetworkTimeout())){
                synchronized(this){
                    if(sqlite){
                        File dataFolder = new File(plugin.getDataFolder(), filename);
                        if (!dataFolder.exists()){
                            try {
                                dataFolder.createNewFile();
                            } catch (IOException e) {
                                MessageUtility.logException(e, this.getClass(), log);
                            }
                        }

                        Class.forName("org.sqlite.JDBC");
                        connection = DriverManager.getConnection("jdbc:sqlite:" + dataFolder);
                        statement = connection.createStatement();
                    }else{
                        Class.forName("com.mysql.cj.jdbc.Driver");
                        connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
                        statement = connection.createStatement();
                    }
                }
            }else
                return;
        } catch (Exception e) {
            MessageUtility.logException(e, getClass(), log);
        }
    }

    public ResultSet executeQuery(String query){
        try {
            if(connection == null || connection.isClosed() || !connection.isValid(connection.getNetworkTimeout())){
                initializeConnection();
            }
            return statement.executeQuery(query);
        } catch (Exception e) {
            MessageUtility.logException(e, getClass(), log);
            return null;
        }
    }

    public boolean execute(String query){
        try {
            if(connection == null || connection.isClosed() || !connection.isValid(connection.getNetworkTimeout())){
                initializeConnection();
            }

            return statement.execute(query);
        } catch (Exception e) {
            MessageUtility.logException(e, getClass(), log);
            return false;
        }
    }

    public PreparedStatement getPreparedStatement(String sql){
        try {
            if(connection == null || connection.isClosed() || !connection.isValid(connection.getNetworkTimeout())){
                initializeConnection();
            }
            return connection.prepareStatement(sql);
        } catch (Exception e) {
            MessageUtility.logException(e, getClass(), log);
            return null;
        }
    }

    public void createTable(String name, String columns){
        execute(String.format("CREATE TABLE IF NOT EXISTS %s (%s);", name, columns));
    }

    public boolean isConnected(){
        try {
            return !connection.isClosed();
        } catch (Exception e) {
            MessageUtility.logException(e, getClass(), log);
            return false;
        }
    }

    public void close(){
        try {
            connection.close();
        } catch (SQLException e) {
            MessageUtility.logException(e, getClass(), log);
        }
    }

}
