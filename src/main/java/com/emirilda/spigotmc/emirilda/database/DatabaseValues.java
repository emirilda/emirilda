package com.emirilda.spigotmc.emirilda.database;

public enum DatabaseValues {

    VARCHAR("VARCHAR(%s) ", 200),
    TINYTEXT("TINYTEXT "),
    TEXT("TEXT "),
    MEDIUMTEXT("MEDIUMTEXT "),
    LONGTEXT("LONGTEXT "),

    BOOLEAN("BOOL "),

    SMALLINT_LIMITED("SMALLINT(%s) ", 100),
    MEDIUMINT_LIMITED("MEDIUMINT(%s) ", 250),
    BIGINT_LIMITED("BIGINT(%s) ", 1000),
    INT_LIMITED("INT(%s) ", 500),
    SMALLINT("SMALLINT "),
    MEDIUMINT("MEDIUMINT "),
    BIGINT("BIGINT "),
    INT("INT "),

    FLOAT_LIMITED("FLOAT(%s) ", 200),
    FLOAT("FLOAT "),

    DOUBLE("DOUBLE "),

    DATE("DATE "),
    DATETIME("DATETIME "),


    PRIMARY_KEY("PRIMARY KEY "),
    AUTO_INCREMENT("AUTO_INCREMENT "),

    NOT_NULL("NOT NULL ");



    private String value;
    private int size;

    DatabaseValues(String value) {
        this.value = value;
    }

    DatabaseValues(String value, int size) {
        this.value = value;
        this.size = size;
    }

    public String get(){
        return String.format(this.value, size);
    }

    public String get(int size){
        return String.format(this.value, size);
    }

}
