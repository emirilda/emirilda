package com.emirilda.spigotmc.emirilda.database;

import com.emirilda.spigotmc.emirilda.utility.ChunkLocation;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.UUID;

public class DatabaseSerializationUtility {
    public static @NotNull String serializeLocation(@NotNull Location location) {
        return String.format("%s;%d,%d,%d", Objects.requireNonNull(location.getWorld()).getUID(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    public static @NotNull Location deserializeLocation(@NotNull String locationString) {
        var splitter = Splitter.onPattern("[;,]").limit(4).trimResults();
        var components = Iterables.toArray(splitter.split(locationString), String.class);
        if (components.length != 4)
            throw new IllegalArgumentException("Invalid location string: " + locationString);
        World world;
        try {
            world = Bukkit.getWorld(UUID.fromString(components[0]));
            if (world == null) {
                throw new Exception();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid world UUID: " + components[0], e);
        }
        var x = Integer.parseInt(components[1]);
        var y = Integer.parseInt(components[2]);
        var z = Integer.parseInt(components[3]);

        return new Location(world, x, y, z);
    }

    public static @NotNull String serializeChunkLocation(@NotNull ChunkLocation chunkLocation) {
        return String.format("%s;%d,%d", Objects.requireNonNull(chunkLocation.getWorld()).getUID(), chunkLocation.getX(), chunkLocation.getZ());
    }

    public static @NotNull String serializeChunkLocation(@NotNull Chunk chunk) {
        return serializeChunkLocation(ChunkLocation.of(chunk));
    }

    public static @NotNull ChunkLocation deserializeChunkLocation(@NotNull String chunkLocationString) {
        var splitter = Splitter.onPattern("[;,]").limit(3).trimResults();
        var components = Iterables.toArray(splitter.split(chunkLocationString), String.class);
        if (components.length != 3)
            throw new IllegalArgumentException("Invalid chunk string: " + chunkLocationString);
        World world;
        try {
            world = Bukkit.getWorld(UUID.fromString(components[0]));
            if (world == null) {
                throw new Exception();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid world UUID: " + components[0], e);
        }
        var x = Integer.parseInt(components[1]);
        var z = Integer.parseInt(components[2]);

        return new ChunkLocation(world, x, z);
    }
}
